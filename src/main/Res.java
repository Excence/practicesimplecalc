package main;

public class Res {
    String val1;
    String val2;
    String oper;

    Res(String val1, String val2, String oper){
        this.val1 = val1;
        this.val2 = val2;
        this.oper = oper;
    }

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }

    public String getOper() {
        return oper;
    }

    public void setOper(String oper) {
        this.oper = oper;
    }
}
