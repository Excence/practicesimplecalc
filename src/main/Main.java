package main;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();

        main.calc(main.parseOneStrIntoThreeStr("33+11"));
        main.calc(main.parseStringIntoObject("55/11"));
    }

    private void calc(String[] st){
        double a = parse(st[0]);
        double b = parse(st[1]);
        switch (st[2]) {
            case "+": parseIntOrDouble(a + b);
                break;
            case "-": parseIntOrDouble(a - b);
                break;
            case "*": parseIntOrDouble(a * b) ;
                break;
            case "/": parseIntOrDouble(a / b);
                break;
            default:
                break;
        }
    }
    private void calc(Res res){
        double a = parse(res.getVal1());
        double b = parse(res.getVal2());
        switch (res.getOper()) {
            case "+": parseIntOrDouble(a + b);
                break;
            case "-": parseIntOrDouble(a - b);
                break;
            case "*": parseIntOrDouble(a * b) ;
                break;
            case "/": parseIntOrDouble(a / b);
                break;
            default:
                break;
        }
    }

    private void parseIntOrDouble(double result){
        if((result % 1) == 0){
            int res = (int) result;
            print("int = " + res);

        }else {
            print("double = " + result);
        }
    }

    private double parse(String count){
        return Double.parseDouble(count);
    }

    private void print(String text){
        System.out.println(text);
    }

    private String[] parseOneStrIntoThreeStr(String str){
        String[] line = new String[3];

        for (char c: str.toCharArray()) {
            if (c == '/' || c=='*' || c=='-' || c=='+'){
                String[] buf = str.split("["+String.valueOf(c)+"]");
                for (int i = 0; i < 2; i++) {
                    line[i] = buf[i];
                }
                 line[2] = String.valueOf(c);
                break;
            }
        }
        return line;
    }

    private Res parseStringIntoObject(String str){
        String[] line = new String[3];

        for (char c: str.toCharArray()) {
            if (c == '/' || c=='*' || c=='-' || c=='+'){
                String[] buf = str.split("["+String.valueOf(c)+"]");
                for (int i = 0; i < 2; i++) {
                    line[i] = buf[i];
                }
                line[2] = String.valueOf(c);
                break;
            }
        }
        return new Res(line[0], line[1], line[2]);
    }
}
